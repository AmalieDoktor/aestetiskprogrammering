# MiniX 10

![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX10/Sk%C3%A6rmbillede_2023-05-01_141516.png)

![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX10/Sk%C3%A6rmbillede_2023-05-01_141543.png)

[Link til program og kode](https://editor.p5js.org/ml5/sketches/CVAE_QuickDraw)

### Hvilken eksempel kode har I valgt, og hvorfor?

Vi har valgt den kode der hedder CVAE(Conditional Variational Autoencoder), den går ud på at man vælger et objekt, og så tegner den det for en. Autoencoder er et netværk der kan skabe en anderledes repræsentation af noget data, f.eks. at et billede kan blive gjort mindre eller mere pixileret så det ikke fylder så meget. Variational autoencoder er en mere avanceret version af dette hvor den kan danne noget helt nyt ud fra dataen og lærer af de modeller den har fået.

Vi har valgt denne kode, da vi synes det var imponerende og spændende at den kunne tegne noget der egentlig var relativt genkendeligt ved at man bare gav et input til hvad man gerne ville have den til at tegne. Den var meget mere avanceret end vi først havde troet, specielt fordi koden er forholdsvis kort (uden JSON filerne). Derfor var den spændende at dykke ned i. 

Programmet giver mulighed for at tegne  nogle forskellige objekter, som man vælger i en dropdown menu, som den kan tegne for en, som fx. airplane, panda, necklace osv. 

Nogle af tegningerne er lettere genkendelige end andre, men det er også lidt spændende at undersøge hvorfor nogle er lettere genkendelige end andre. Det giver et indblik i hvordan den lærer fra billeder, og hvilke ting der nemme at kendekende og hvilke der ikke er. 

### Har I ændret noget i koden for at forstå programmet? Hvilke?

I starten af koden ser vi nogle variabler, efterfulgt af setup og et kanvas som vi kender det. I vores setup bliver der også lavet en createButton, som er den der står ‘generate’ på. Der bliver også henvist til en JSON fil, som har alle ordene man kan vælge i programmet er beskrevet. Derefter kommer der en function der hedder modelReady, som der bliver brugt til at lave de forskellige kategorier. 

Vi prøvede f.eks. at ændre i dropdown hvor der var en syntakse der hedder createSelect. Ved at ændre i den, fandt vi ud af at den ‘dropdown menu’ hvor man kan vælge et ord forsvinder. Så createSelect laver den dropdown function vi kan se. 

Efterfølgende kommer der en funktion der hedder generateImage, som fungerer når knappen bliver trykket på, hvor den så bruger valuen fra dropdown menuen, og derefter begynder at lave billederne. 

Vi forsøgte os også i at se om vi kunne give den nogle flere funktioner til den button() der allerede var lavet. Så vi har lavet endnu en funktion, (function baggrund), som er et mouseClicked, hvor de bruger mousePressed ved deres, vi har gjort så den nye funktion tegner en ny baggrund, når man trykker på knappen. Men fordi deres er et mousePressed, gjorde det sådan at når man holder knappen inde, kan man stadig se tegningen, men når man giver slip, kommer der en ny baggrund. Vi lavede denne ændring for at lære om den måde de havde lavet deres Button, var essentiel for at computeren ville vide, hvad den skulle tegne og hvor den skulle tegne det. Vi lærte at computeren stadig ville forstå hvad den skulle hente, selv efter denne ændring. Selv efter vi ændrede hvor den skulle tegne Image’et, og fik den til at bevæge sig langs x-aksen med hvert tryk, tegnet den en ny figur hver gang..

### Hvilke linjer af kode har været særlig interessante for gruppen? Hvorfor?

Det har de linjer af kode, som vi også har ændret på, da det har været dem, vi ikke har forstået. 

Så de stykker kode og syntakser vi har beskrevet ovenover. 


### Hvordan ville I udforske/udnytte begrænsningerne af eksempelkoden eller machine learning algoritmer? 

En af de begrænsninger, der kunne dukke op med denne type neurale netværk, kunne være hvis der bliver spurgt om at tegne et objekt, som netværket ikke er trænet på. Med det eksempelkode vi har arbejdet ud fra, har netværket en vifte af objekter som det kan “tegne”. Men det kan ikke skabe nye objekter, hvis ikke først netværket er blevet trænet på eksempler af den ønskede type. Der er altså ikke fuldstændigt frie tøjler fra brugerens side, til at få genereret lige det man ønsker sig. 

Lige nu er de billeder man ser også relativt pixeleret, og ikke så skarpe at se på, altså nogle af dem kan være en smule svære at se hvad forestiller. Men hvis man giver et billede af hvordan en panda skal se ud, så trækker den bare fra det billede, i stedet for at benytte google quick draw, så vil det ikke være machine learning længere. Altså der er begrænsninger på hvordan billederne kommer til at se ud hvis det stadig skal kategoriseres som machine learning. 


### Var der syntakser/funktioner, som I ikke kendte til før? Hvilke? Og hvad lærte I?

Som nævnt tidligere, så kendte vi ikke til createSelect. Men den funktion kan lave dropdown menuer som vi kan se i programmet. 

Så er der selvfølgelig CVAE, som vi heller ikke har arbejdet meget med førhen. Conditional Variational Autoencoder skaber de her generative modeller og så lærer den ved at indkode data af pixileret billeder og så kan den ud fra det danne et ‘generelt’ billede af en genstand. 

### Hvordan kan I se en større relation mellem eksempel koden, som I har undersøgt, og brugen af machine learning ude i verdenen

I det program vi har valgt, så har den evnen til at ‘skabe’ noget ud fra en beskrivelse, som er det vi vælger i dropdown menuen. Det er noget vi kender til fra AI og lignende, som f.eks. laver billeder eller videoer. De kan også skabe noget ud fra en beskrivelse, selvfølgelig lidt mere avanceret, men det her program er nok grundstenene for at sådan noget kan fungere. 


### Hvad kunne du tænke dig at vide mere om? Eller hvilke spørgsmål kan du ellers formulere ift. emnet?

Json filer, hvordan man udnytter en Json fil ordentligt. Da vi er lidt forvirrede over hvornår det er en fordel at bruge Json fremfor en class, eller bare at skrive variable in i sin js fil.

Fx i denne CVAE hvor de benytter en Json fil til de variable der er, altså de forskellige objekter som den kan tegne. Så her ville vi jo egentlig tænke at man bare kunne sætte disse variable ind på sin js fil som en let= …. alle de variable, og ved ikke helt hvorfor det er en fordel at have det på en Json fil. 

Så ja, vi ville gerne vide lidt mere om Json filer og hvornår de er en fordel at bruge, og hvornår det ikke er nødvendigt. 

_Hvornår er det en fordel at benytte sig af en Json fil?_

Derudover er det også spændende at undersøge hvornår det går fra at “almindelig” kode til at være machine learning. Hvor meget skal man ændre på fx den kode i CVAE for at det ikke længere er machine learning, men bare et stykke kode man beder dem om at køre. 

_Hvor går grænsen mellem machine learning og opsat kode?_ 

## link til de andres gitlab
[Isak's gitlab](https://gitlab.com/IsakiSensei/aestetisk-programmeing)

[Emil's gitlab](https://gitlab.com/sommerpilgaard99/aestetisk-programmering)

[Signe's gitlab](https://gitlab.com/SigneAM/aestetisk-programmering/-/tree/main)




