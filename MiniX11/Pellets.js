class Pellets {
    constructor(_x, _y, _r) {
        this.x = _x;
        this.y = _y;
        this.r = _r;

    }

    show() {
        push();
        rectMode(CENTER);
        fill(0, 0, 255);
        image(antivirus, this.x, this.y, this.r, this.r);
        pop();
    }

}