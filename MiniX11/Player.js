class Player {

  constructor(_x, _y, _r) {
    this.pos = createVector(_x,_y)
    this.r = _r;
  }
  show(img,sizeMod) {
    //Makes the Entity display images. The size modifier is to make it easier
    // to chage the size of the sprites.
    imageMode(CENTER);
    image(img,this.pos.x,this.pos.y,sizeMod*r,sizeMod*r);
  }


  run(top,bottom,left,right) {
    if(top == false) {
      if(keyIsDown(83) || keyIsDown(40)){ //83 is the code for the s key. 40 is Down
        this.pos.y++;
      }
    }
    if(bottom == false) {
      if(keyIsDown(87) || keyIsDown(38)){ //87 is the code for the w key. 38 is Up
        this.pos.y--;
      }
    }
    if(right == false) {
      if(keyIsDown(65) || keyIsDown(37)){ //65 is the code for the a key. 37 is Left
        this.pos.x--;
      }
    }
    if (left == false) {
      if(keyIsDown(68) || keyIsDown(39)){ //68 is the code for the d key. 39 is Right
        this.pos.x++;
      }
    }
  }

  edges() {
    if (this.pos.x > width + this.r) {
      this.pos.x = -this.r;
    } else if (this.pos.x < -this.r) {
      this.pos.x = width + this.r;
    }
    if (this.pos.y > height + this.r) {
      this.pos.y = -this.r;
    } else if (this.pos.y < -this.r) {
      this.pos.y = height + this.r;
    }
  };
}