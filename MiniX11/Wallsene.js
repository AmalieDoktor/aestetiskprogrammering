class Wall{
 constructor(_x, _y, _w, _h){
    this.x = _x;
    this.y = _y;
    this.w = _w;
    this.h = _h;

 }

 show () {
   push();
   rectMode(CORNER);
   stroke(255,200,0);
   fill(0,0,255);
    rect(this.x, this.y, this.w, this.h);
    pop();
 }

}