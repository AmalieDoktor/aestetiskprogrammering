class Zuck{

    constructor(){
      rectMode(CENTER);
      imageMode(CENTER);
      this.posX = random (45, 1110);
      this.posY = random(45,350);
      this.speed = 10;
      this.size = 50;
    }

    show(){
        push();
        noFill();
        fill(255);
        image(zuckerberg, this.posX, this.posY, this.size, this.size);
        pop();
        noFill();
        rect(this.posX, this.posY, this.size, this.size);
       
    }

    move(){
        this.posX = this.posX + random(this.speed);
        this.posX = this.posX - random(this.speed);
        this.posY = this.posY + random(this.speed);
        this.posY = this.posY - random(this.speed);


    }
}
