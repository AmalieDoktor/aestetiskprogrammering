let zuck = [];
let antal = 10;
let wall = [];
let antalw = 1;
let player;

let r = 20;

let pellets = [];

let wWidth = [];
let wXPos; let wYPos;
let topSide; let bottomSide; let rightSide; let leftSide;

let antalp = 2;

let startx = 600;
let starty = 300;

let button;
let button2;
let button3;

let inp;

let trophy;

let score = 0;
let froggy;

function preload() {

  zuckerberg = loadImage("zucker.png");
  players = loadImage("fbpfp.png");
  antivirus = loadImage("AntiVirus.png");
  mark = loadImage("Mark.png");
  trophy = loadImage("trophy.png");
  soundFormats('mp3');
  froggy = loadSound("froggy.mp3");
}

function setup() {
  createCanvas(1200, 600);
  background(0);
  for (let i = 0; i < antal; i++) {
    zuck.push(new Zuck());
  }
  player = new Player(startx, starty, r);
  
  wallSetup();
  pelletSetup();
  froggy.play();

}

function draw() {
  
  background(0);
  displayScore();
  spawnWalls();
  thePlayer();
  spawnZuck();
  collsion();
  spawnPellets();
  collisionCheck();
  collisionCheckZuck();

  for (let i = 0; i < wall.length; i++) {
    wall[i].show();
  }

  if (keyIsDown(LEFT_ARROW)) {
    player.pos.x -= 2;
  }

  if (keyIsDown(RIGHT_ARROW)) {
    player.pos.x += 2;
  }

  if (keyIsDown(UP_ARROW)) {
    player.pos.y -= 2;
  }

  if (keyIsDown(DOWN_ARROW)) {
    player.pos.y += 2;
  }
}

function thePlayer() {
  player.show(players, 2);
  player.edges();
  player.run(topSide, bottomSide, leftSide, rightSide);
}

function spawnPellets() {
  for (let i = 0; i < pellets.length; i++) {
    pellets[i].show();
  }
}

function pelletSetup() {
  pellets[0] = new Pellets(55, 55, 35);
  pellets[1] = new Pellets(55, 140, 35);
  pellets[2] = new Pellets(55, 220, 35);
  pellets[3] = new Pellets(255, 55, 35);
  pellets[4] = new Pellets(255, 140, 35);
  pellets[5] = new Pellets(255, 220, 35);
  pellets[6] = new Pellets(355, 55, 35);
  pellets[7] = new Pellets(355, 140, 35);
  pellets[8] = new Pellets(355, 220, 35);
  pellets[9] = new Pellets(520, 55, 35);
  pellets[10] = new Pellets(520, 220, 35);

  ///////////////////////////////////////////////////////////////////////////////////////////////
  pellets[11] = new Pellets(1145, 55, 35);
  pellets[12] = new Pellets(1145, 140, 35);
  pellets[13] = new Pellets(1145, 220, 35);
  pellets[14] = new Pellets(945, 55, 35);
  pellets[15] = new Pellets(945, 140, 35);
  pellets[16] = new Pellets(945, 220, 35);
  pellets[17] = new Pellets(845, 55, 35);
  pellets[18] = new Pellets(845, 140, 35);
  pellets[19] = new Pellets(845, 220, 35);
  pellets[20] = new Pellets(680, 55, 35);
  pellets[21] = new Pellets(680, 220, 35);

  ///////////////////////////////////////////////////////////////////////////////////////////////
  pellets[22] = new Pellets(55, 545, 35);
  pellets[23] = new Pellets(55, 460, 35);
  pellets[24] = new Pellets(55, 380, 35);
  pellets[25] = new Pellets(255, 545, 35);
  pellets[26] = new Pellets(255, 460, 35);
  pellets[27] = new Pellets(255, 380, 35);
  pellets[28] = new Pellets(355, 545, 35);
  pellets[29] = new Pellets(355, 460, 35);
  pellets[30] = new Pellets(355, 380, 35);
  pellets[31] = new Pellets(520, 545, 35);
  pellets[32] = new Pellets(520, 380, 35);

  ////////////////////////////////////////////////////////////////////////////////////////////////////
  pellets[33] = new Pellets(1145, 545, 35);
  pellets[34] = new Pellets(1145, 460, 35);
  pellets[35] = new Pellets(1145, 380, 35);
  pellets[36] = new Pellets(945, 545, 35);
  pellets[37] = new Pellets(945, 460, 35);
  pellets[38] = new Pellets(945, 380, 35);
  pellets[39] = new Pellets(845, 545, 35);
  pellets[40] = new Pellets(845, 460, 35);
  pellets[41] = new Pellets(845, 380, 35);
  pellets[42] = new Pellets(680, 545, 35);
  pellets[43] = new Pellets(680, 380, 35);
}

function spawnZuck() {
  for (let i = 0; i < zuck.length; i++) {
    zuck[i].show();
    zuck[i].move();
  }
}

function spawnWalls() {
  for (let i = 0; i < wall.length; i++) {
    wall[i].show();
  }
}

function wallSetup() {

  displayScore();
  // Venstre hjørne ydre væg 
  wall[0] = new Wall(0, 0, 545, 30);
  wall[1] = new Wall(545, 0, 30, 80);
  wall[2] = new Wall(0, 0, 30, 275);
  wall[3] = new Wall(0, 245, 80, 30);

  // Højre hjørne ydre væg
  wall[4] = new Wall(625, 0, 555, 30);
  wall[5] = new Wall(625, 0, 30, 80);
  wall[6] = new Wall(1170, 0, 30, 255);
  wall[7] = new Wall(1125, 245, 80, 30);

  //Venstre nedre hjørne ydre væg
  wall[8] = new Wall(0, 570, 575, 30);
  wall[9] = new Wall(545, 525, 30, 45);
  wall[10] = new Wall(0, 325, 30, 285);
  wall[11] = new Wall(0, 325, 80, 30);

  // Højre nedre hjørne ydre væg
  wall[12] = new Wall(655, 570, 550, 30);
  wall[13] = new Wall(625, 525, 30, 80);
  wall[14] = new Wall(1170, 325, 30, 285);
  wall[15] = new Wall(1125, 325, 80, 30);

  //////////////////////////////////////////////////////////////////////////////////////////////////////

  //  nedre hjørne ydre væg
  wall[16] = new Wall(80, 80, 150, 35);
  wall[17] = new Wall(80, 165, 150, 30);
  wall[18] = new Wall(280, 80, 50, 115);
  wall[19] = new Wall(380, 80, 115, 115);
  wall[20] = new Wall(130, 245, 200, 30);
  wall[21] = new Wall(380, 245, 195, 30);
  wall[22] = new Wall(545, 130, 30, 115);

  //  nedre hjørne ydre væg
  wall[23] = new Wall(970, 80, 150, 35);
  wall[24] = new Wall(970, 165, 150, 30);
  wall[25] = new Wall(870, 80, 50, 115);
  wall[26] = new Wall(705, 80, 110, 115);
  wall[27] = new Wall(870, 245, 200, 30);
  wall[28] = new Wall(625, 245, 195, 30);
  wall[29] = new Wall(625, 130, 30, 115);

  //  nedre hjørne ydre væg
  wall[30] = new Wall(80, 485, 150, 35);
  wall[31] = new Wall(80, 405, 150, 30);
  wall[32] = new Wall(280, 405, 50, 115);
  wall[33] = new Wall(380, 405, 115, 115);
  wall[34] = new Wall(130, 325, 200, 30);
  wall[35] = new Wall(380, 325, 195, 30);
  wall[36] = new Wall(545, 355, 30, 115);


  //  nedre hjørne ydre væg
  wall[37] = new Wall(970, 485, 150, 35);
  wall[38] = new Wall(970, 405, 150, 30);
  wall[39] = new Wall(870, 405, 50, 115);
  wall[40] = new Wall(705, 405, 110, 115);
  wall[41] = new Wall(870, 325, 200, 30);
  wall[42] = new Wall(625, 325, 195, 30);
  wall[43] = new Wall(625, 355, 30, 115);


}

function mousePressed() {
  console.log(mouseX, mouseY);
}

function collsion() {
  for (let i = 0; i < wall.length; i++) {
    if (player.pos.y + r >= wall[i].y && player.pos.y - r <= wall[i].y + wall[i].h) {
      if (player.pos.x + r >= wall[i].x && player.pos.x - r <= wall[i].x + wall[i].w) {
        console.log(player.pos.x + r, player.pos.y + r, "væg nr ", i, wall[i]);
        if (player.pos.y < wall[i].y) {
          topSide = true;
          player.pos.y -= 3;
          topSide = false;
          console.log("top", i);
        } else if (player.pos.y > wall[i].y + wall[i].h) {
          bottomSide = true;
          player.pos.y += 3;
          bottomSide = false;
          console.log("bottom", i);
        }
        if (player.pos.x < wall[i].x) {
          leftSide = true;
          player.pos.x -= 3;
          leftSide = false;
          console.log("left", i);
        } else if (player.pos.x > (wall[i].x + wall[i].w)) {
          rightSide = true;
          player.pos.x += 3;
          rightSide = false;
          console.log("right", i);
        }
      }

    }
  }
}

function collisionCheck() {
  let distance;
  for (let i = 0; i < pellets.length; i++) {
    distance = dist(player.pos.x, player.pos.y, pellets[i].x, pellets[i].y)

    if (distance < r / 2 + pellets[i].r / 2) {
      pellets.splice(i, 1);
      console.log("Colected", i);
      score++;
    }
  }

}

function displayScore() {
  fill(250,200,0);
  textSize(17);
  textAlign(CENTER);
  textFont("courier New");
  text('Du har samlet '+ score + "VPN", width/2, height/2);

  if (score === 44) {
    push();
    noLoop();
      fill(255);
      let col = color(0);
      button = createButton("Klik her for at se din pris");
      button.style('color', '#ffffff');
      button.style('font-size', '30px');
      button.style('background-color', col);
      button.position(380, 250);
      button.size(435, 100);
      button.mousePressed(youWin);
      pop();
      froggy.stop();
  }
}

function youWin(){
  createCanvas(1200, 600);
  imageMode(CENTER);
  background(255);
  image(trophy, width/2, 400);

  button.hide();

  push();
  fill(0);
  textSize(50);
  textFont('courier New');
  textAlign(CENTER);
  text("Du har succesfuldt samlet al VPN!", width/2, 100);
  pop();

  push();
  fill(0);
  textSize(50);
  textFont('courier New');
  textAlign(CENTER);
  text("Spillet er vundet!", width/2, 200);
  pop();
 
}

function collisionCheckZuck() {
  let distance;
  for (let i = 0; i < zuck.length; i++) {
    distance = dist(player.pos.x, player.pos.y, zuck[i].posX, zuck[i].posY)

    if (distance < r / 2 + zuck[i].size / 2) {
      noLoop();
      zuck.splice(i,1);
      push();
      fill(255);
    
      let col = color(0);
      button = createButton("Fortsæt?");
      button.style('color', '#ffffff');
      button.style('font-size', '30px');
      button.style('background-color', col);
      button.position(380, 250);
      button.size(435, 100);
      button.mousePressed(youDead);
      pop();
      froggy.stop();
    }
  }
}

function youDead() {
  createCanvas(1200, 600);
  imageMode(CORNER);
  background(mark);
  button.hide();

  button2 = createButton("ENTER");
  button2.position(320,460);
  button2.size(100,50);
  button2.mousePressed(reset);

  button3 = createButton("Jeg vil ikke svare");
  button3.position(790,460);
  button3.size(100,50);
  button3.mousePressed(sceneskift)


  push();
  fill(0);
  textSize(40);
  textFont('courier New');
  textAlign(CENTER);
  text("Svar på dette spørgsmål for at leve videre", width/2, height/4);
  pop();

  push();
  fill(0);
  textAlign(CENTER);
  textSize(25);
  textFont('courier New');
  text("Hvad er dit Kontonummer", width/2, height/2.3);
  pop();
  inp = createInput('');
  inp.position(400, 300);
  inp.size(400);
}

function sceneskift(){
  background(mark);

  fill(0);
  textSize(50);
  textAlign(CENTER);
  textFont('courier New');
  text('Du har tabt', width/2, 200);
  textSize(25);
  text('Vi har nu din data! Men bare rolig, vi skal nok passe godt på den..', width/2, height/2);
  
 button2.hide();
 button3.hide();
 inp.hide();

}

function reset(){
 button.hide();
 button2.hide();
 button3.hide();
 inp.hide();

 loop();
 spawnZuck();
 froggy.play();
 redraw();
}


