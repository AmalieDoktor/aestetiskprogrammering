# MiniX 3

screenshot af MiniX 3:

![](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX3/Sk%C3%A6rmbillede_2023-03-03_140150.png)

[se miniX 3 her](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX3/index.html)

[link til source code](https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/blob/main/MiniX3/minix3.js)


### What do you want to explore and/or express? (idefasen)
Min tanke i denne MiniX var at lave en throbber til et spil, eller i hvert fald inspireret af spil. Så imens spillet loader og starter et map, så kører denne throbber. Siden der i nogen skydespil er en radar, så tænkte jeg det kunne være interessant at prøve at lave en throbber der er inspireret af dette, dog ikke som et kort, blot en radar som man måske kender fra skibe eller fly. Det var et forsøg på at gøre throbberen relateret til spillet og gøre den interessant at kigge på. 

Ideen var at få en `rect()` til at køre rundt i en cirkel med `rectMode()` sat til CORNER, og så vil der nogle gange komme en prik frem ligeså snart `rect()` kører forbi (som om at radaren har opdaget noget). Jeg lavede radaren med at sammensætte en masse `ellipser()`.

```
push()
    translate(width/2, height/2);
    fill(70,80);
    ellipse(0, 0, 30);
    ellipse(0, 0, 404);
    ellipse(0, 0, 304);
    ellipse(0, 0, 204);
    ellipse(0, 0, 104);
    line(153, 133, 0, 0);
    line(50, 194, 0, 0);
    line(-60, 194, 0, 0);
    line(-154, 130, 0, 0);
    line(-200, 25, 0, 0);
    line(-190, -70, 0, 0);
    line(-120, -162, 0, 0);
    line(-20, -200, 0, 0);
    line(90, -180, 0, 0);
    line(175, -100, 0, 0);
    line(200, 30, 0, 0);
    pop()

```
`rect()` får man til at rotere ved at bruge en variable og `frameCount()` som jeg vil komme mere ind på senere. 

```
pop()
     let num = 90;
     push();
     translate(width/2, height/2);
     let cir = 360/num*(frameCount%num);
     rotate(radians(cir));
     rectMode(CORNER);
     noStroke();
     fill(200, 255, 0);
     rect(5, -5, 202, 10);
     pop();

```


### What are the time-related syntaxes/functions that you have used in your program [...]

```
if(frameCount %90 == 0) 
    {
    noStroke();
    fill(200, 255, 0);
    ellipse(900,350,30);
    }

```

Som nævnt tidligere, så har jeg brugt `frameCount()`, som er en time-related syntakse. I det her tilfælde begynder den at tælle ligeså snart programmet starter. Så den tæller alle frames, og når den så når til frame 90 bliver det `if()` statement jeg har lavet sandt. I dette tilfælde bruges til at få prikken til at blinke hver gang `rect()` kommer hen over det og giver illusionen af at radaren har set noget. Men i virkeligheden har jeg bare fået prikken til at blinke på et bestemt tidspunkt, hvor jeg så har sat prikken et bestemt sted, så det giver illusionen af at den kommer frem hver gang radaren 'scanner'. Siden jeg har sat `frameCount()` til at være 30 fps, så går det langsommere end hvis jeg f.eks. havde sat den til 60 fps.

Jeg ville gerne have haft prikken til at være der i længere tid, så jeg begyndte at arbejde lidt med baggrunden og `fill()`, men i sidste ende havde jeg desværre ikke nok tid til at få det til at lykkedes. I hvert fald ikke uden at det blev hakket eller mærkeligt, så det er noget jeg må undersøge til næste gang. 


### Think about a throbber that you have encounted in digital culture [...] 
Jeg oplever tit throbbers, f.eks. i spil som jeg nævnte, eller til en youtube video. Men en af de steder hvor jeg har haft en tanke omkring throbbers er når jeg redigerer videoer i et software program der hedder After Effects. Nogle gange, så crasher After Effects hvis min computer ikke kan følge med. Når skærmen freezer og der IKKE er en throbber, så ved jeg at den nok skal komme til at fungere igen. Så skal jeg bare lige vente lidt og så begynder programmet formodentligt at virke igen efter kort tid. Men hvis der kommer en throbber istedet for min mus, så ved jeg at programmet er helt crashed og nok bliver nødt til at starte forfra. Det er lidt ironisk, når throbberen viser at computeren 'tænker', men i det her tilfælde er jeg mere tålmodig når throbberen IKKE er der end når den er der. Her kan man nævne temporalitet i software/netværk kultur som er noget vi har diskuteret på klassen. Siden vi hele tiden bliver bombaderet med data og forventer at alle sider og videoer online kører _med det samme_ at vi åbner dem, så er hele pointen i throbbers lidt væk. Fordi jeg _ved_ at After Effect ikke kan løse problemet og at der inden længe kommer en meddelelse, der spørger om jeg vil lukke programmet, så bliver jeg utålmodig og lukker programmet inden den kan få lov til at tænke færdig. Det er selvfølgelig ikke godt at gøre, men vi er efterhånden blevet så vant til at tingene går så hurtigt at throbbers ikke 'bør' være der længere end nødvendigt.
Så, i en verden hvor tingene går så hurtigt som de gør, så kan man nok godt forestille sig at throbbers er mere irriterende end de måske var før i tiden, siden vi ser dem så sjældent nu til dags. Det sætter i hvert fald nogle tanker i gang hos mig, fordi jeg forestiller mig at vi i fremtiden måske slet ikke ser throbbers?
