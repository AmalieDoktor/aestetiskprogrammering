//throbber
function setup() {
    //create a drawing canvas
    createCanvas(windowWidth, windowHeight);
    frameRate (30);  //try to change this parameter
   }
   
   function draw() {
     background(100, 10);  //check this syntax with alpha value
     drawElements();
     push()
     noStroke();
     fill(200, 255, 0);
     ellipse(width/2, height/2, 20);
     pop();
   }
   
   function drawElements() {
    push()
    translate(width/2, height/2);
    fill(70,80);
    ellipse(0, 0, 30);
    ellipse(0, 0, 404);
    ellipse(0, 0, 304);
    ellipse(0, 0, 204);
    ellipse(0, 0, 104);
    line(153, 133, 0, 0);
    line(50, 194, 0, 0);
    line(-60, 194, 0, 0);
    line(-154, 130, 0, 0);
    line(-200, 25, 0, 0);
    line(-190, -70, 0, 0);
    line(-120, -162, 0, 0);
    line(-20, -200, 0, 0);
    line(90, -180, 0, 0);
    line(175, -100, 0, 0);
    line(200, 30, 0, 0);
    pop()
    push()
    if(frameCount %90 == 0) 
    {
    noStroke();
    fill(200, 255, 0);
    ellipse(900,350,30);
    }
    pop()
     let num = 90;
     push();
     translate(width/2, height/2);
     let cir = 360/num*(frameCount%num);
     rotate(radians(cir));
     rectMode(CORNER);
     noStroke();
     fill(200, 255, 0);
     rect(5, -5, 202, 10);
     pop();

    }
   