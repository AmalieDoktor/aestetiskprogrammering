# MiniX 4

#### Provide a title for and a short description of your work (1000 characters or less) as if you were going to submit it to the festival.
Har du nogensinde logget på online og faktisk set eller forstået hvad der foregår inde bag din skærm? Hver gang du klikker på et link, en side eller blot på en knap på dit tastatur, ved du så helt præcis hvad du går ind til? Og ikke mindst, ved du hvad du lader fremmede mennesker vide om dig? Mit projekt er et spil, som man bliver en del af hver gang man går online. Med fire valg muligheder og absolute ingen viden om hvad der sker når du trykker på en bestemt knap, så har du ingen mulighed for at beskytte dig selv eller dine data fra at blive delt på nettet. Men er det blot et spil eller er det noget der foregår lige nu, mens du læser den her tekst?

Prøv det selv:

[Klik her for at se MiniX 4](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX4/index.html)

(HVIS LINKET IKKE VIRKER; PRØV AT ÅBNE DET I FIREFOX)

[Source code](https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/blob/main/MiniX4/minix4.js)



#### Describe your program and what you have used and learnt.
Mit program er lidt opstillet som et spil, men hvor det er meget begrænset hvad man kan gøre for at undgå at ens data bliver 'leaked'. Der står i toppen et spørgsmål, og så er der nedenunder 4 knapper som man kan trykke på. Når man trykker på en knap, så kommer der en boks frem hvor der så står beskrevet hvad der er sket efter at have trykket på knappen.

Her er to screenshots af eksempler på hvordan det ser ud når du har trykket på en knap.

![](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX4/sk%C3%A6rmbillede_2.png)
![](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX4/sk%C3%A6rmbillede_4.png)

Og her er et screenshot af 'startskærmen', altså der hvor knapperne er:

![](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX4/forside.png)

Måden jeg gjorde det på var ved at have nogle `text()` funktioner hvor jeg skrev spørgsmålet, og så brugte jeg syntaksen `createButton()` til at lave knapperne.

Det gjorde jeg sådan her:

```
button1 = createButton('1');
button1.position(50, 300);
button1.size(70, 50);
button1.mousePressed(valg1);

```
Ved hjælp af syntakser og variabler kunne jeg lave en knap, hvorefter jeg så kunne lave en function der bestemmer hvad der sker ligeså snart knappen bliver trykket på (altså, `mousePressed()` funktionen):

```
function valg1(){
    push()
    fill(200);
    rectMode(CENTER);
    rect(240, 350, 300, 200);
    textSize(32);
    pop()
    push()
    fill(250, 0, 0);
    text('WARNING', 187, 300);
    text('Data breach', 180, 340);
    textSize(15);
    text('your webcam data has been leaked', 120, 360);
    pop()
    push()
    capture = createCapture(VIDEO);
    capture.position(190, 375);
    capture.size(80, 60);
    pop()
    button1.hide();
    button2.hide();
    button3.hide();
    button4.hide();
```
En syntakse jeg ikke har brugt før er `hide()`, som er rigtig god hvis man oplever at boksen f.eks. ikke dækker alle knapperne til, og at man gerne vil have dem til at forsvinde. Ved at sætte hide efter de forskellige variabler brugt til knapperne, så forsvinder de ligeså snart man trykker på en af knapperne.

At bruge funktioner til at bestemme hvad der sker når man trykker på knapperne minder meget om `if()` statements og er meget effektive at bruge. 

Et eksempel på dette er: Når man trykker på en af knapperne er det ens 'webcam data' der bliver 'leaked', så når man trykker på den knap, så begynder kameraet at optage. Der har jeg brugt `createCapture(VIDEO)`, som bruger computerens webcam til at optage en video/livefeed. Jeg havde i tankerne at gøre det samme med lyd, men desværre kunne jeg ikke få det til at lykkedes indenfor det tidsrum jeg havde at arbejde med.

#### Articulate how your program and thinking address the theme of “capture all.”
Ideen med 'capture all' er at vi lever i et samfund hvor alt kan blive set som data og det er noget vi lever i, i mange tilfælde uanset om vi vil det eller ej. Man kan beskrive det lidt ved at bruge begrebet 'datafication' som Mejias, Ulises & Couldry snakker om i deres tekst (2019):

"Rather, datafication is a contemporary phenomenon which refers to the quantification of human life through digital information, very often for economic value."

Det er hele tiden data der bliver opfanget, selv bare hvis du eksisterer, for så er du måske en del af en klasse, en arbejdsforening, et samfund eller lignende som der hele tiden bliver bygget data på. Hver gang du går online er du udstillet, også selvom du ikke selv vælger at være det.

Det er noget af det mit program har fokus på, nemlig det her med at brugeren sjældent har en chance for at passe på sig selv når de går online medmindre de har erfaring eller viden indenfor 'data capture'. Det er der desværre ikke mange der har, og derfor opstår der tit utrygge episoder online og man kan være uheldig at få sine data misbrugt. Mit program er et forsøg på at sætte sig lidt kritisk overfor det og give en visualisering af hvor svært det kan være for en normal person at beskytte sig selv online, når alt de kan se og ved er at der er 4 knapper og de skal trykke på en af dem. Det er det samme online: Vi trykker på et link, men vi kan ikke se hvad der sker udover at det åbner en hjemmeside - men ofte sker der en helt del mere.



#### What are the cultural implications of data capture?

Data capture og data generalt bliver beskrevet godt af Kitchin (2015) i teksten fra Mejias et al.(2019):

"Data is the “material produced by abstracting the world into categories, measures and other representational forms [...] that constitute the building blocks from which information and knowledge are created”"

Data er noget der bliver skabt omkring mennesker, samfundet og lignende som bliver kogt ned til tal, viden og kategorier. Vi snakkede på klassen om 'rå data', som faktisk ikke rigtig er et begreb der findes. Data er nemlig altid en abstraktion af noget. For at kunne skabe data fra helt almindelige situationer eller mennesker, så bliver man nødt til at gøre noget kvalitativt til noget kvantitativt. Det synes jeg er hele pointen med data capture - nemlig at kunne indfange og benytte så mange informationer om en person, uanset om de er kvalitative eller kvantitative. Data er alting og alting er data.
