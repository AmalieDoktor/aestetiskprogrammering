let button1;
let button2;
let button3;
let button4;
let capture;
let mic;
let img;
function preload(){
    img = loadImage('../MiniX4/lydbillede.png')
}

function setup(){
createCanvas(500, 500);
background(255, 0, 0, 70);

button1 = createButton('1');
button1.position(50, 300);
button1.size(70, 50);
button1.mousePressed(valg1);

button2 = createButton('2');
button2.position(150, 300);
button2.size(70, 50);
button2.mousePressed(valg2);

button3 = createButton('3');
button3.position(250, 300);
button3.size(70, 50);
button3.mousePressed(valg3);

button4 = createButton('4');
button4.position(350, 300);
button4.size(70, 50);
button4.mousePressed(valg4);






}

function draw(){
textSize(50);
text('CHOOSE WISELY', 45, 100);
textSize(20);
text('you only have one chance', 120, 150);
        

    }

function valg1(){
    push()
    fill(200);
    rectMode(CENTER);
    rect(240, 350, 300, 200);
    textSize(32);
    pop()
    push()
    fill(250, 0, 0);
    text('WARNING', 187, 300);
    text('Data breach', 180, 340);
    textSize(15);
    text('your webcam data has been leaked', 120, 360);
    pop()
    push()
    capture = createCapture(VIDEO);
    capture.position(190, 375);
    capture.size(80, 60);
    pop()
    button1.hide();
    button2.hide();
    button3.hide();
    button4.hide();


}

function valg2(){
  
    push()
    fill(200);5
    rectMode(CENTER);
    rect(250, 350, 300, 200);
    textSize(32);
    pop()
    push()
    fill(250, 0, 0);
    text('WARNING', 190, 300);
    text('Data breach', 180, 340);
    textSize(15);
    text('your audio data has been leaked', 130, 360);
    pop()
    image(img, 140, 390, 200, 40);
    button1.hide();
    button2.hide();
    button3.hide();
    button4.hide();

}

function valg3(){

    push()
    fill(200);
    rectMode(CENTER);
    rect(240, 350, 300, 200);
    textSize(32);
    pop()
    push()
    fill(250, 0, 0);
    text('WARNING', 190, 300);
    text('Data breach', 180, 340);
    textSize(15);
    text('your IP address has been leaked', 130, 360);
    pop()
    button1.hide(); 
    button2.hide();
    button3.hide();
    button4.hide();

}

function valg4(){

push()
fill(200);
rectMode(CENTER);
rect(240, 350, 300, 200);
textSize(32);
pop()
push()
fill(0, 100, 0);
text('COMPUTER SECURED', 130, 300);
text('congratulations', 165, 340);
textSize(15);
text('your network is now secured', 140, 360);
pop()
button1.hide();
button2.hide();
button3.hide();
button4.hide();



}



    
