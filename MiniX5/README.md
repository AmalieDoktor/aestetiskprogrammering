# MiniX 5


[Link til program](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX5/index.html)

[Link til souce code](https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/blob/main/MiniX5/minix5.js)

###### Skærmbilleder af program

Uden trammer:
![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX5/uden_gitter.png)


Trammer der løber ned over:
![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX5/halvt_med_gitter.png)


Trammer der dækker:
![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX5/gitter_fyldt_ud.png)

Mit program skal forestille en labyrint der bliver lavet via `rect()`, som bare fortsætter og bliver tegnet ovenpå hinanden. Man skal forestille sig at labyrinten er i 3D format og mens den bliver dannet, så begynder nogle trammer at køre nedover labyrinten. Jeg vil komme mere ind over hvorfor jeg har valgt at gøre dette i reflektionerne længere nede til de senere spørgsmål.

Måden jeg har lavet trammerne på har været ved et for-loop, der lyder sådan her:
```
for (let linje = 20; linje <= 1000; linje += 30) {
    line(linje, 0, linje, y); }
```
Måden jeg har lavet labyrinten på var ved at tage udgangspunkt i den kode vi har lavet på klassen, som ser sådan her ud:
```
stroke(0, noise(xoff) * 100, 255);
    strokeWeight(2);
    xoff += 0.5;
    const Random1 = random(1);
    if (Random1 < 0.5) {
        fill(255);
        rect(x, y, x + 20, y + 20);

    } else {
        line(x, y + 20, x + 20, y);
    }
```
Her kan man se at der er en `stroke()` som er det der laver linjerne, samt en `strokeWeight()` der bestemmer hvor 'tykke' stregerne er. Der er også en `if()` statement, som siger noget om hvor linjen skal være og en 'else' statement som sker hvis `if()` statementet ligesom ikke bliver true. 
Der er også en constant, som jeg vil komme ind på i spørgsmålet nedenunder.

#### What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?

Der er brugt nogle regler i mit program for at få det til at have den her 'infinity' fornemmelse. Jeg har blandt andet brugt en constant, som gør at denne variable ikke kan ændre sig. Derfor sætter den ligesom en 'regel' for at programmet ikke kan ændre denne variable.
```
const Random1 = random(1);
```
Jeg har kaldt den for Random1 og brugt syntaksen `random()` og sat den til 1.

Man kan også sige jeg har sat en anden regel for programmet, nemlig at hver gang linjerne når udersiden af canvaset (altså, det yderste punkt på x-aksen), så skal den starte med at køre på næste linje.
```
 x += 20;
    if (x > width) {
        y = y + 20;
        x = 0;
```

Hvis man bare lader mit program kører, så kommer der flere og flere 'gange' i labyrinten, og den kan potentielt fortsætte forevigt hvis jeg havde lavet mit canvas større. Samtidig, så sørger mine regler for at labyrinten altid er anderledes, så der er aldrig to gange der er ens. Siden jeg har min constant med random, så laver den hver gange random formater og gange der bare fortsætter i evighed.


#### What role do rules and processes have in your work?
Reglerne skaber, som nævnt tidligere, lidt den her randomness, altså at labyrinten hele tiden ændrer sig, hvilket nærmest gør den umulig at slippe ud af. Jeg ville også gerne have gjort det sådan at trammerne ligesom bevæger sig 'hurtigere' ned, så man ikke rigtig kan slippe fri fra dem. Men det kunne jeg desværre ikke få til at fungere, så det må være en opgave for en anden gang.

Siden jeg også har brugt den her `if()` statement der siger at labyrinten skal fortsætte på næste linje, uanset hvor stort canvaset er, så vil det også være umuligt at stoppe den her process af at bygge labyrinten.

#### Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” [...]
Jeg synes den her ide om 'auto-generator' er spændende, specielt hvis man tager fat i randomness delen af det. Randomness skaber kunst, hvor man så kan diskutere om 'rigtig' randomness faktisk eksisterer. Hvis man tager fat i denne sætning fra vores læsning:

_"Once the outcome of a game is known, the game becomes meaningless. Incorporating chance into the game helps delay the 
moment when the outcome will become obvious"_ (Montfort et al. p. 122(2012))

Så kan man snakke om at randomness på sin måde vil kunne løse den her 'meningløshed', fordi folk ikke længere kunne regne ud hvad der kommer næst. Til gengæld kan man så diskutere om konstant randomness skaber endnu mere meningløshed en f.eks. et spil der er efter en bestemt storyline. F.eks. i mit program, der vil der ikke være nogen ende på labyrinten. Den vil altid fortsætte og man vil ikke kunne regne ud hvilken række den laver næst. Den eneste regl du kan regne med er, at trammerne kommer, men alt andet er random. Så kan man jo argumentere for at uendelighed uden nogen viden for hvad der kommer næst er mere spændende, men næsten ligeså meningsløst. Auto-generators er noget der kører uden menneskeligt input, så derfor kan hele den her labyrint med trammerne virke som noget man ikke kan bryde ud af. Programmet kører uanset om mennesker vil have det eller ej. Det skaber det her loop og konstante fornemmelse af at man ikke kan komme ud af det.
