let x = 0;
let y = 0;
let xoff = 1;

function setup() {
    createCanvas(700, 700);
    background(200);
    
}


function draw() {

    stroke(0, noise(xoff) * 100, 255);
    strokeWeight(2);
    xoff += 0.5;
    const Random1 = random(1);
    if (Random1 < 0.5) {
        fill(255);
        rect(x, y, x + 20, y + 20);

    } else {
        line(x, y + 20, x + 20, y);
    }

    x += 20;
    if (x > width) {
        y = y + 20;
        x = 0;

    }
    push()
    translate(-150, -150);
    stroke(100);
    strokeWeight(10);
    for (let linje = 20; linje <= 1000; linje += 30) {
    line(linje, 0, linje, y); }
    pop()
    


}










