# MiniX 6

### Describe how does/do your game/game objects work?

[link til program](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX6/index.html)

[link til source code 1](https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/blob/main/MiniX6/MiniX6.js)

[link til source code 2](https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/blob/main/MiniX6/firkant.js)

![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX6/Sk%C3%A6rmbillede_2023-03-24_121226.png)

![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX6/Sk%C3%A6rmbillede_2023-03-24_121301.png)

![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX6/Sk%C3%A6rmbillede_2023-03-24_121317.png)

Jeg valgte at gå med en meget simpel approach i forhold til at kode et spil, da jeg ikke har haft mulighed for at deltage i undervisningen i denne uge. Mit spil går ud på, at man skal klikke på firkanten så mange gange som muligt inden tiden løber ud (rekorden på spillet lige nu er 94 points/klik!). Min første tanke var, at når man trykker på firkanten, så går scoren op og når tiden er løbet ud, så stopper spillet. Jeg havde desværre ikke tid til at dykke dybt ned i at sørge for at det kun er indenfor firkanten man kan trykke, eller at pointene stopper ligeså snart spillet stopper. 
Men overordenet set, så har jeg lavet koden nedenfor, som gør at pointene går op ved hjælp af `mouseClicked()` funktionen.
```
function mouseClicked(){
if (score === 0) {
    score++;
} else {
    score++;
} }

```
Jeg har brugt en variable til at lave scoren, som så går op hver mange 'mouse is clicked'. Jeg har brugt en variable til at lave tiden, som er samme princip som med score. At i stedet for ++, så sætter man -- og så bliver der talt ned. Derefter brugte jeg et conditional statement til at beskrive hvornår spillet er færdigt og når man skal starte op igen:
```
if(spilStart){
    if(time > 0) {
        square.showfirkant();
        if(frameCount % 60 == 0) {
            time--;
        }
}else{
    text("game over", width/2, height/2);
    }
}else{
    text("start spil", width/2, height/2);
}
}
```

Firkanten kommer jeg ind på i næste afsnit.
Som nævnt tidligere, så er der nogle mangler i mit spil. F.eks. at du kan få point uanset hvor henne på canvaset du trykker. Samtidig, så kan du blive ved med at optjene point selvom spillet er stoppet, så længe du bare bliver ved med at trykke. Måden jeg nok ville have løst disse to problemer på, var ved at lave nogle conditional statements. Til første eksempel ville jeg sørge for at det kun er inde for bestemte områder man kan trykke. I forhold til at få point efter spillet er slut, kunne jeg nok også have lavet et conditional statement eller måske benyttet mig af 'hide' på en eller anden måde. Desværre, så har jeg ikke haft tiden til at få prøvet det af. 

### Describe how you program the objects and their related attributes, and the methods in your game.
Jeg synes det var rigtig svært at bruge de her forskellige metoder til at lave objekterne og jeg sad med det rigtig længe. I sidste ende blev det noget meget simpelt jeg gik med for at lave firkanten. Jeg forsøgte at lave en class med nogle forskellige værdier og benytte construktor til at sætte det hele op.

```
class Square {
    constructor(){
        this.x = width/2;
        this.y = height/2;
        this.size = 100;
        this.color = color(255);
    }
showfirkant(){
    fill(this.color);
    rectMode(CENTER);
    rect(this.x, this.y, this.size, this.size);
}


}
```
Derefter brugte jeg de her forskellige værdier til at vise firkanten. Det lykkedes, men jeg prøvede mig egentlig bare frem og vidste ikke helt om opsætningen var rigtig. Jeg havde taget udgangspunkt i noget af de vi lavede på klassen, men i en meget simpel udgave. Jeg synes det var rigtig svært at forstå og sad med det længe, men heldigvis fik jeg det til at fungere.

### Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?

I gennem de tekster vi har læst har jeg fået opfattelsen, at object-oriented programming har meget at gøre med fokus på objektor i stedet for en helhed, og samtidig en måde at organisere store projekter. I teksten 'the obscure objects of object orientation' beskriver Matthew Fuller og Andrew Goffey den her ide med at fjerne objectet fra subjectet:
_"The act of encapsulation is the act of removing from the object its direct relation to the environtment"_ (s. 37), som er noget vi f.eks. gjorde på klassen ved at fjerne forskellige objekter fra koden og sætte det ind i en ny fil, men at de to seperate filer stadig åbner sammen. Så igen, så virker det lidt som en måde hvorpå man kan organisere og gøre store projekter mere håndterlige end førhen.
Det beskriver samme tekst også sådan her:
_"the technical features of object-orientated programming have not just influenced the organization of code; they have also had significant impact on the organization of work."_ (s. 50). 


### Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?
I en af teksterne vi skulle læse, der hedder 'object abstraction' af Soon Winnie & Cox, forklarer de den her tanke med 'abstraction' og sammenhængen med at organisere og skabe flow i software/code. En af de udtryk det beskriver det med lyder sådan: _"Abstraction can be thought of as a process of simplification, in which a complex entity is reduced to a more manageable form"_ (s. 143). 
Lige i forhold til min kode, der gav det ikke så meget mening at dykke ned i 'abstraction' eller 'simplification' da min kode er forholdsvis simpel i forvejen. Jeg har f.eks. kun 1 firkant i mit spil, derfor var hele pointen med class nok ikke så nødvendigt. Men til det vi lavede på klassen virkede 'class' og generelt 'abstraction' til at være en god måde at adskille objekter fra koden og derfor skabe en mere overskuelig kode. Ved at dele det op og skille de forskellige ting ad, så gør du noget meget uoverskueligt og complex til noget nemt. Når man blot er et menneske, så kan lange koder med mange forskellige funktioner dræne motivationen og gøre det svært at arbejde med, derfor er det altid smart at lære at organisere fra starten af, så man undgår de fejl og mangler der tit opstår i uoverskuelige filer. 

