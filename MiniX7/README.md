# MiniX 7

[Klik her for programmet](https://amaliedoktor.gitlab.io/aestetiskprogrammering/MiniX7/index.html)

[klik her for source code 1](https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/blob/main/MiniX7/MiniX7.js)

[Klik her for source code 2](https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/blob/main/MiniX7/firkant.js)

![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX7/Sk%C3%A6rmbillede_2023-04-01_143046.png)

![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX7/Sk%C3%A6rmbillede_2023-04-01_143137.png)

![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX7/Sk%C3%A6rmbillede_2023-04-01_143118.png)




## Hvilken miniX har du valgt?
Jeg har valgt at bygge videre på min MiniX6, da jeg fik noget feedback på den som jeg synes var spændende at give mit i kast med!

## Hvilke ændringer har du lavet og hvorfor?
Jeg har lavet to store ændringer. 

1. I mit første spil fik man point i takt med at man trykker på firkanten, men hvor at man stadig kunne få flere point efter tiden var løbet ud. Det har jeg ændret nu ved at ændre to ting i koden.

Den tidligere kode:
```
function mouseClicked(){

if (score === 0) {
    score++;
} else {
    score++;
} 
}
```

Den nuværende kode:
```
function mouseClicked(){
    if (time > 0){
    score++; 
}
```
Så med hjælp fra instruktorne fandt jeg ud af at man bare kan bruge tiden i stedet for at bruge scoren til at få pointene til at stoppe med at tælle efter tiden er løbet ud.

I første kode tænkte jeg at når scoren var 0, så skulle pointene gå op hver gang ´mouseClicked´. Men når man så havde trykket på knappen 1 gang, så stoppede pointene med at tælle opad. Derfor, så lavede jeg en 'else' i mit statement som gjorde at uanset hvad scoren var === med, så vil pointene blive ved med at vokse når musen blev trykket.

I anden kode gjorde jeg så noget helt andet og sagde at så længe tiden er større end 0, så kan man få point ved at klikke på musen. 

2. Den anden ting jeg ændrede i koden var at man kun kan få point hvis man trykker indenfor firkanten. Dette gjorde jeg ved at lave et `if()` statement, som så forudsager at det kun er inde for nogle bestemte områder at ´mouseClicked´ kan ske.

Koden ser sådan her ud:
```
function mouseClicked(){
if(mouseX > 200 && mouseX < 300 && mouseY > 200 && mouseY < 300){
    if (time > 0){
        score++;   
} }
```
Så inden for de rammer der hedder 200-300 for musens x-position og inden for 200-300 af musens y-position kan man få point.

3. en lille ekstra ting jeg valgte at ændre var at min firkant bliver mindre hver gang man trykker på den, nemlig i forsøget på at få det til at blive sværre nu flere gange man trykker:

Dette gjorde jeg ved at lave et `if()` statement igen og tage fat i nogle af værdierne fra min class.
```
if(mouseClicked){
    square.size -=0.5;
}
```
Den eneste ulempe ved dette er, at det ikke lykkeds mig at få værdierne for mouseX og mouseY til at følge med når firkanten bliver mindre. Derfor er der på nogle tidspunkter til sidst når firkant er lille at man godt kan få point ved at klikke uden for firkanten. 

Hvad jeg ville have gjort for at løse dette problem er nok at bruge noget andet end class, nemlig bare have nogle variabler for firkanten som jeg så vil sætte i stedet for f.eks. 200 og 300 værdier for mouseX og mouseY. Så lave nogle variabler som man også kan bruge til at få firkantens størrelse til at ændre sig, og så få dem til at følge med hinanden. 

## Hvad har du lært i denne miniX?
At tænke i andre retninger. Når jeg kigger på min første kode og det første punkt jeg ændrede, så synes jeg virkelig at det var dumt noget af det første jeg havde lavet. Det virker som et meget nemt problem, med af en eller anden grund havde jeg slet ikke tænkt over at jeg kunne løse problemet så nemt. Derfor var det fedt at kigge tilbage på min tidligere miniX og virkelig blive klar over at nogle ting er meget nemmere at løse end man lige går at tror. Jeg tror jeg blev bange for at ændre på noget jeg ved der allerede virkede. 

## Hvad er relationen mellem æstetisk programmering og digital kultur?
Jeg synes der er nogle points i hovedbogen som beskriver æstetisk programmering til at være er forsøg på at udfordre eller bygge oven på den digitale kultur (Soon & Cox). Digital kultur er jo noget vi lever med i vores hverdag, hvor æstetisk programmering prøver at gøre noget creativt og anderledes ved denne digitale kultur. Så på samme side, så går de to betegelser hånd i hånd, da æstestetisk programmering tit er en udbygning eller forbedring af den digitale kultur der allerede er. Dog kan man måske også se æstetisk programmering som et kritisk modsvar til den digitale kultur vi allerede kender til.

Lige i forhold til min MiniX, så synes jeg at den tager fat om hele pointen med underholdning i æstetisk programmering. En måde de siger det på i hovedbogen er: "rather than simply seeing software as a tool for carrying out pre-exsisting instructions, aesthetic programming emphasizes the creative possibilities of programming as a mode of expression and exploration" (Soon & Cox, s. 1). Jeg synes dette giver mening i forhold til spil, da spil ikke er en nødvendighed for os. Det er blot noget vi laver for underholdning eller 'exploration'. Min MiniX har ikke noget højere formål end at være sjov og interessant at prøve.

