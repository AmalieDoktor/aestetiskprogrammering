[link til .js fil](https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/blob/main/MiniX9/runme9.js)

[Link til .json fil](https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/blob/main/MiniX9/House.json)

[Link til program](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX9/index.html) (hvis linket ikke virker, åben i firefox!)

![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX9/Sk%C3%A6rmbillede_2023-04-24_134505.png)
![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX9/Sk%C3%A6rmbillede_2023-04-24_134446.png)
![](https://AmalieDoktor.gitlab.io/aestetiskprogrammering/MiniX9/Sk%C3%A6rmbillede_2023-04-24_134408.png)

## Provide a title of your work and a short description
#### Vores titel: Potter stereotyperne. 

Description: Vi har lavet et program, der er inspireret af serien Harry Potter. Pointen i programmet er at man kan klikke på en af de forskellige 'huse' fra Harry Potter som er Ravenclaw, Gryffindor, Hufflepuff og Slytherin. Når man klikker på en af husene, så kommer der en beskrivelse af 'stereotypen' af personerne der bor i dette hus. F.eks. Holdning til skole, hvilke personlighedstræk f.eks. om man er modig, en pleaser eller lignende. I forhold til navnet, så havde vi tænkt, at det kunne være interessant at benytte code og teksten i koden til at beskrive nogle stereotyper, der faktisk sætter mennesker i Harry Potter i bås. Både for at beskrive hvordan Harry Potter fungerer og bliver set, men også for at kommentere på den måde menneskene i Harry Potter bliver kategoriseret og sat i bås, og om det er en fordel at blive sat i hus sammen med nogle der minder rigtig meget om en selv. 

## Describe how your program works, and what syntax you have used, and learnt?
Vi har selvfølgelig benyttet os af en JSON file, som er noget nyt vi har givet os i kast med. Det er en god måde at strukturere opsætningen af vores kode. Vi har også brugt funktionen `mousePressed()`, som bliver brugt til at klikke på de forskellige skjold. Vi har før brugt `mouseClicked()` og `mouseIsPressed`, men i det her tilfælde gik vi med `mousePressed()` for at teksten forsvinder efter man har trykket. 
Vi starter hele processen med at lave en masse variabler, også nogen der henviser til JSON filen. Derefter har vi loaded nogle billeder af de skjold/huse vi skal bruge. Så har vi gennem vores `if()`-statements gjort det muligt at klikke skjoldene og på den måde få nogle stereotyper frem for det hus. Nedenfor ser du et eksempel på hvordan sådan en `if()`-statement ser ud. 

```
if (mouseX > 1085 && mouseX < 1470 && mouseY > 0 && mouseY < 385) {
    clear();
    background(0);
    text(person2, width/2, height/2);
```

I vores JSON fil har vi så beskrevet og lavet skjoldene med fokus på teksten der kommer ligeså snart et skjold bliver trykket på. Klik på linket ovenfor for at se JSON filen. Vi forsøgte at lave sætninger i vores JSON fil, men hver gang man lavede et mellemrum, så gik ordet en linje ned. Vi kunne simpelthen ikke finde ud af hvordan vi kunne løse det, så vi lavede et alternativ i form af at bruge underscore_ i stedet for mellemrum. På den måde kunne vi stadig have fulde sætninger hver gang der trykkes på en af billederne.


## Analyze and articulate your work/How would you reflect on your work in terms of Vocable Code?
En bedre måde at kunne udtrykke sig med sprog i sin kode. Måden de beskriver det på i teksten er sådan her:
"Code is both script and performance, and in this sense is always ready to do something: it says what it will do, and does it at the same time" - (Vocable code, Winnie & Cox).

Man kan sætte sig mere kritisk til ting i sin JSON fil, fordi man kan beskrive præcis hvad man gerne vil udtrykke. Det kan man ikke nødvendigvis i en normal JS fil. Man kan sige at kode på en eller anden måde altid har et udtryk eller noget kritisk i sig, men det er typisk ikke nemt for ‘normale’ mennesker at se dette (Som er kendetegnet ved Vocable Code). Et godt udtryk for at forklare dette kan lyde sådan: 
"In this particular arrangement, the source code is no longer, as in the convention of most software, hidden from the user and is instead displayed in full to undermine the implied hierarchy between the source and its results" (vocable code, Winnie & Cox). Man kan ligge et stykke kode ud, men ikke forvente at folk forstår hvilken kritik eller kunst der ligger bag det. Med en mere direkte og ‘nem forståelig’ kode, som f.eks. JSON filen, så er det nemmere for programmøren/kunstneren at komme ud med sit budskab. I vores tilfælde bruger vi vores tekst i JSON filen til at sætte nogle stereotyper op omkring de forskellige grupper i Harry Potter. 

Hele ideen med at sætte folk i bokse er jo noget man kan sætte sig kritisk overfor. Når man ser skjoldet fra f.eks. Slytherin, så er der en slange på skjoldet og med det samme tænker man at slytherin er en dårlig ting. Hvilket er noget vores værk prøver at vise - at der er nogle stereotyper bag hvert hus, og at ligeså snart du har valgt at klikke på en, så har du påduttet dig selv nogle stereotyper fra det hus. Hvis man tog det udgangspunkt at personen der åbner vores program ikke kender til Harry Potter, så har de ikke en særlig stor chance for at vide hvilke stereotyper de kommer til at have. Så, folk får nogle stereotyper, de måske faktisk ikke kan lide og det er noget vores program vil vise på en kritisk måde gennem kode. 
Vi valgte at blive inspireret af Harry Potter, da det er en meget stor serie og der er en god ‘community’ rundt om den. Der findes der også mange spil med Harry Potter, der er meget populære, hvor man også bliver inddelt i husene. Og derfor har folk nok allerede sat sig selv i en boks og sat sig under de stereotyper, der er i de forskellige huse. Af den grund er det spændende at prøve at vise hvad folk egentlig siger om sig selv, og hvordan de bliver set af andre. 

## link til de andres gitlab
[Isak's gitlab](https://gitlab.com/IsakiSensei/aestetisk-programmeing)

[Emil's gitlab](https://gitlab.com/sommerpilgaard99/aestetisk-programmering)

[Signe's gitlab](https://gitlab.com/SigneAM/aestetisk-programmering/-/tree/main)




