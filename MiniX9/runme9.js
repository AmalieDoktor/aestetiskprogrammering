let myJson;
let index = 0;

let y = 0;

let gryffindor;
let hufflepuff;
let slytherin;
let ravenclaw;

function preload() {
    myJson = loadJSON("House.json");
    gryffindor = loadImage("Gryffindor_house_crest.png");
    hufflepuff = loadImage("Hufflepuff_house_crest.png");
    slytherin = loadImage("Slytherin_house_crest.png");
    ravenclaw = loadImage("Ravenclaw_house_crest.png");

}

function setup() {
    createCanvas(1470, 770);
    background(0);
    mousePressed();
    frameRate(30);

}

function draw() {
    fill(255);
    image(gryffindor, 0, 0, 385, 385);
    image(hufflepuff, 1085, 0, 385, 385);
    image(slytherin, 0, 385, 385, 385);
    image(ravenclaw, 1085, 385, 385, 385);



}

function mousePressed() {
    textFont("Arial Black")
    textSize(25);
    textWrap(WORD);
    textAlign(CENTER);

    let person = myJson.Gryffindor[index].jegEr;
    let person2 = myJson.Hufflepuff[index].jegEr;
    let person3 = myJson.Slytherin[index].jegEr;
    let person4 = myJson.Ravenclaw[index].jegEr;


    if (mouseX > 0 && mouseX < 385 && mouseY > 0 && mouseY < 385) {
        clear();
        background(0);
  text(person, width/2, 300, 0);
}

if (mouseX > 1085 && mouseX < 1470 && mouseY > 0 && mouseY < 385) {
    clear();
    background(0);
    text(person2, width/2, 250, 0);
  }

  if (mouseX > 0 && mouseX < 385 && mouseY > 385 && mouseY < 770) {
    clear();
    background(0);
    text(person3, width/2, 250, 0);
  }

  if (mouseX > 1085 && mouseX < 1470 && mouseY > 385 && mouseY < 770) {
    clear();
    background(0);
    text(person4, width/2, 250, 0);
    y++;

  }
}